const canvas = document.getElementById("gameOfLifeCanvas");
const ctx = canvas.getContext("2d");
const speedSlider = document.getElementById("speedSlider");
const zoomSlider = document.getElementById("zoomSlider");
const timerButton = document.getElementById("timerButton");
const clearButton = document.getElementById("clearButton");
const min = zoomSlider.min;
const max = zoomSlider.max;
let scale = 40;
let speed = 1000;
let timer = null;
let renderposition = [0, 0];
let mouseposition = null;

const resize = () => {
  canvas.width = document.documentElement.clientWidth * 0.9;
  canvas.height = document.documentElement.clientHeight - 300
  drawAll();
};

const zoom = (value) => {
  if (value < min) {
    value = min;
  } else if (value > max) {
    value = max;
  }
  scale = Math.floor(value);
  zoomSlider.value = scale;
  drawAll();
}

window.onresize = resize;

canvas.onpointerdown = (event) => {
  const x = Math.floor(event.offsetX);
  const y = Math.floor(event.offsetY);
  const [orx, ory] = renderposition;
  mouseposition = [x, y, orx, ory];
  event.preventDefault();
};

canvas.onpointermove = (event) => {
  if(mouseposition != null) {
    const x = Math.floor(event.offsetX);
    const y = Math.floor(event.offsetY);
    const [ox, oy, orx, ory] = mouseposition;
    renderposition = [orx - ox + x, ory - oy + y];
    drawAll();
  }
};

canvas.onpointerup = (event) => {
  const [ox, oy,,] = mouseposition;
  mouseposition = null;
  let x = Math.floor(event.offsetX);
  let y = Math.floor(event.offsetY);
  if(Math.floor(ox) === x
  && Math.floor(oy) === y) {
    const [rx, ry] = renderposition;
    x = Math.floor((x - rx) / scale);
    y = Math.floor((y - ry) / scale);
    const c = check(life, cell(x, y));
    if (c === undefined) {
      life.push(cell(x, y));
    } else {
      life.splice(life.indexOf(c), 1);
    }
  }
  drawAll();
};

canvas.onwheel = (event) => {
  zoom(Math.floor(scale + (event.wheelDelta / 20)));
  event.preventDefault();
};

const cell = (x, y) => {
  return {
    getX: () => x,
    setX: value => x = value,
    getY: () => y,
    setY: value => y = value
  }
};

const heat = (x, y, nc, alive) => {
  return {
    getX: () => x,
    setX: value => x = value,
    getY: () => y,
    setY: value => y = value,
    getNC: () => nc,
    setNC: value => nc = value,
    getAlive: () => alive,
    setAlive: value => alive = value
  }
};

let life = [
  cell(1, 4),
  cell(2, 4),
  cell(3, 4),
  cell(3, 3),
  cell(2, 2),
];

const drawSquare = (point, border = 1) => {
  const [rx, ry] = renderposition;
  ctx.fillStyle = "#eeeeee";
  ctx.fillRect(rx + point.getX() * scale + border, ry + point.getY() * scale + border, scale - border * 2, scale - border * 2);
};

const drawLine = (x0, y0, x1, y1) => {
  ctx.beginPath();
  ctx.lineWidth = "2";
  ctx.strokeStyle = "#607D8B";
  ctx.moveTo(x0, y0);
  ctx.lineTo(x1, y1);
  ctx.stroke(); 
}

const drawHorizontalLine = (y) => {
  const [, ry] = renderposition;
  drawLine(0, y + (ry % scale), canvas.width, y + (ry % scale));
};

const drawVerticalLine = (x) => {
  const [rx,] = renderposition;
  drawLine(x + (rx % scale), 0, x + (rx % scale), canvas.height);
};

const drawAll = () => {
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  life.forEach(p => {
    drawSquare(p, scale / 20);
  });
  // draw lines
  const [rx, ry] = renderposition;
  let hc = 0;
  while(hc <= canvas.height + scale) {
    drawHorizontalLine(hc);
    hc += scale;
  }

  let vc = 0;
  while(vc <= canvas.width + scale) {
    drawVerticalLine(vc);
    vc += scale;
  }
};

const drawHeatmap = (next) => {
  next.forEach(c => {
    let count = c.getNC();
    switch (count) {
      case 0:
        ctx.fillStyle = "#FF0000";
        break;
      case 1:
        ctx.fillStyle = "#FF5000";
        break;
      case 2:
        ctx.fillStyle = "#FFA000";
        break;
      case 3:
        ctx.fillStyle = "#FFF000";
        break;
      default:
        ctx.fillStyle = "#AAFAA0";
        break;
    }
    drawSquare(c, scale / 5);
    ctx.fillStyle = "#eeeeee";
  });
}

const check = (array, c) => {
  // undefined or cell
  return array.find(p => p.getX() === c.getX() && p.getY() === c.getY());
};

const loop = () => {
  let next = [];
  // mark all alive cells
  life.forEach(l => {
    next.push(heat(l.getX(), l.getY(), 0, true));
  });
  // add +1 around each alive cell
  let tmp = next;
  tmp.forEach(c => {
    for (let x = -1; x < 2; x++) {
      for (let y = -1; y < 2; y++) {
        // skip alive cell
        if (!(x === 0 && y === 0)) {
          // check cell exists
          const r = next.find(p => p.getX() === c.getX() + x && p.getY() === c.getY() + y);
          if (r === undefined) {
            next.push(heat(c.getX() + x, c.getY() + y, 1, false));
          } else {
            // Add +1
            r.setNC(r.getNC() + 1);
          } 
        }
      }
    }
  });
  // Check each cell
  life = [];
  next.forEach(c => {
    if(c.getNC() === 3 || c.getAlive() && c.getNC() === 2) {
      // Check if cell is alive because of 3 neighbor rule
      // Check if alive cell should stay alive because of 2 neighbor rule
      life.push(cell(c.getX(), c.getY()));
    }
  });
  // draw new life
  drawAll();
};

const start = () => {
  timer = setInterval(() => {
    loop();
  }, speed);
  timerButton.innerHTML = "pause";
};

const pause = () => {
  clearInterval(timer);
  timer = null;
  timerButton.innerHTML = "continue";
};

speedSlider.onchange = event => {
  speed = 5000 - event.target.valueAsNumber;
  if(timer !== null) {
    pause();
    start();
  }
};

zoomSlider.onchange = event => {
  zoom(event.target.valueAsNumber);
};

zoomSlider.oninput = event => {
  zoom(event.target.valueAsNumber);
};

timerButton.onclick = () => {
  if(timer === null) {
    start();
  } else {
    pause();
  }
};

clearButton.onclick = () => {
  if(timer !== null) {
    pause();
    life = [];
    start();
  } else {
    life = [];
  }
  drawAll();
};

resize();
drawAll();
start();
loop();
